# Blog CI Kata

Simple blog engine - java Spring Boot rest api as backend, react.js as frontend

## Backend

### Run build
```
./gradlew build
```

### Run service

Service listens http://localhost:8080/

```
./gradlew runBoot
```

## Frontend

### Install dependencies
```
npm install
```

### Build 
```json
npm run watch
```

