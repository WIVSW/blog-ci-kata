package ru.agilix.blog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.agilix.blog.model.Entry;
import ru.agilix.blog.repository.EntryRepository;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(EntryRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Entry("first post", "some text", "author")));
            log.info("Preloading " + repository.save(new Entry("second post", "some another text", "author")));
        };
    }
}