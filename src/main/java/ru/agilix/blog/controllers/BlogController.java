package ru.agilix.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.agilix.blog.model.Entry;
import ru.agilix.blog.repository.EntryRepository;

import java.util.List;

@RestController
public class BlogController {
    private final EntryRepository repository;

    public BlogController(EntryRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/entries")
    List<Entry> all() {
        return repository.findAll();
    }

}
