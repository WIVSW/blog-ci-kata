package ru.agilix.blog.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Entry {
    private @Id @GeneratedValue Long id;
    private String tile;
    private String text;
    private String author;

    public Entry() { }

    public Entry(String tile, String text, String author) {
        this.tile = tile;
        this.text = text;
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTile() {
        return tile;
    }

    public void setTile(String tile) {
        this.tile = tile;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.tile, this.text);
    }

    @Override
    public String toString() {
        return "Entry{" + "id=" + this.id + ", name='" + this.tile + '\'' + ", author='" + this.author + '\'' + '}';
    }
}
