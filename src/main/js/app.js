const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {entries: []};
    }

    componentDidMount() {
        client({method: 'GET', path: '/entries'}).done(response => {
            this.setState({entries: response.entity});
        });
    }

    render() {
        return (
            <EntryList entries={this.state.entries}/>
        )
    }
}

class EntryList extends React.Component {
    render() {
        const entries = this.props.entries.map(entry =>
            <Entry key={entry.id} entry={entry}/>
        );
        return (
            <table width="100%">
                <tbody>
                    <tr>
                        <th>
                            <h1>Blog</h1>
                        </th>
                    </tr>
                    {entries}
                </tbody>
            </table>
        )
    }
}

class Entry extends React.Component {
    render() {
        return (
            <tr>
                <td align="center">
                    <table>
                        <tbody>
                            <tr>
                                <td>{this.props.entry.tile}</td>
                                <td><a href="#">@{this.props.entry.author}</a></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    {this.props.entry.text}
                                    <hr/>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
        )
    }
}

ReactDOM.render(
    <App/>
    ,
    document.getElementById('react')
)