package ru.agilix.blog;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.agilix.blog.controllers.BlogController;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class BlogControllerCiKataApplicationTests {

    @Autowired
    private BlogController controller;

    @Test
    void blogControllerNotEmpty()  {
        assertThat(controller).isNotNull();
    }

}
